package com.aboo.framework.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * 项目名称：  ttsframe   
 * 类名称：  StatusEnum   
 * 描述：  存放成功失败状态的枚举
 * @author  娄安   
 * 创建时间：  2015年10月13日 下午1:19:35 
 * 修改人：娄安    修改日期： 2015年10月13日
 * 修改备注：
 *
 */
public enum StatusEnum {
	
	SUCCESS("Y", "成功"),
	FAILURE("N", "失败");
	
	private String value;
	private String text;
	
	private StatusEnum(String value, String text) {
		this.value = value;
		this.text = text;
	}
	
	public static Map<String, String> getMap() {
		Map<String, String> map = new HashMap<String, String>();
		StatusEnum[] systemArr = StatusEnum.values();
		for (StatusEnum system : systemArr) {
			map.put(String.valueOf(system.getValue()), system.getText());
		}
		
		return map;
	}
	
	public static String getText(String value) {
		StatusEnum[] systemArr = StatusEnum.values();
		for (StatusEnum system : systemArr) {
			if (value.equals(system.getValue()))
				return system.getText();
		}
		
		return null;
	}
	
	public static StatusEnum getEnum(String value) {
		StatusEnum[] systemArr = StatusEnum.values();
		for (StatusEnum system : systemArr) {
			if (value.equals(system.getValue()))
				return system;
		}
		
		return null;
	}

	public String getValue() {
		return value;
	}

	public String getText() {
		return text;
	}
	
}
