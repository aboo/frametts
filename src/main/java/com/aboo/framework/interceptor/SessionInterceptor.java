package com.aboo.framework.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 
 * 项目名称：  ttsframe-web   
 * 类名称：  SessionInterceptor   
 * 描述：  拦截器示例，根据实际情况自行调整
 * @author  娄安   
 * 创建时间：  2015年10月21日 上午8:46:44 
 * 修改人：娄安    修改日期： 2015年10月21日
 * 修改备注：
 *
 */
public class SessionInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object obj) throws Exception {
		Object currentUser = request.getSession().getAttribute("currentUser");
		if (currentUser != null) {
			return true;
		}

		response.sendRedirect("login.do");
		return false;
	}

}
