package com.aboo.framework.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 
 * 项目名称：  ttsframe   
 * 类名称：  ApplicationContextUtil   
 * 描述：  获取Spring容器中的JavaBean工具类
 * @author  娄安   
 * 创建时间：  2015年10月13日 上午10:18:44 
 * 修改人：娄安    修改日期： 2015年10月13日
 * 修改备注：
 *
 */
public class ApplicationContextUtil implements ApplicationContextAware {

	private static ApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext act)
			throws BeansException {
		applicationContext = act;
	}	
	
	public static Object getBean(String beanName) {
		return applicationContext.getBean(beanName);
	}
	
	public static <T> T getBean(Class<T> clazz) {
		return applicationContext.getBean(clazz);
	}
	
	public static <T> T getBean(String beanName, Class<T> clazz) {
		return applicationContext.getBean(beanName, clazz);
	}

}
