package com.aboo.framework.util.httpclient;

import java.util.concurrent.TimeUnit;

import org.apache.http.impl.conn.PoolingClientConnectionManager;

public final class HttpConnectionClear implements Runnable {
	private PoolingClientConnectionManager cm;

	public HttpConnectionClear(PoolingClientConnectionManager cm) {
		this.cm = cm;
	}

	public void run() {
		// System.out.println("清理前http连接数："+cm.getTotalStats().getAvailable());
		try {
			this.cm.closeExpiredConnections();
			this.cm.closeIdleConnections(1, TimeUnit.SECONDS);
		} catch (Exception e) {
			// TODO: handle exception
		}

		// System.out.println("清理后http连接数："+cm.getTotalStats().getAvailable());
	}

}
