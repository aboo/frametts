package com.aboo.framework.vo;

import com.aboo.framework.constant.StatusEnum;

/**
 * 
 * 项目名称：  ttsframe   
 * 类名称：  ReturnVO   
 * 描述：  返回值的基础类
 * @author  娄安   
 * 创建时间：  2015年10月13日 下午1:14:41 
 * 修改人：娄安    修改日期： 2015年10月13日
 * 修改备注：
 *
 */
public class ReturnVO <T> {
	
	private String flag;
	private String failReason;
	private String extend;
	private T data;
	
	public String getFlag() {
		return flag;
	}
	public void setFlag(StatusEnum status) {
		this.flag = status.getValue();
	}
	public String getFailReason() {
		return failReason;
	}
	public void setFailReason(String failReason) {
		this.failReason = failReason;
	}	
	public String getExtend() {
		return extend;
	}
	public void setExtend(String extend) {
		this.extend = extend;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	
}
