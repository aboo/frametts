package com.aboo.sample.example1.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aboo.sample.example1.dao.Sample1DAO;
import com.aboo.sample.example1.dao.Sample1Mapper;
import com.aboo.sample.example1.pojo.Sample1VO;

/**
 * 
 * 项目名称：  ttsframe-web   
 * 类名称：  SampleService   
 * 描述：  示例Service，仅为测试服务
 * @author  娄安   
 * 创建时间：  2015年10月21日 上午8:48:28 
 * 修改人：娄安    修改日期： 2015年10月21日
 * 修改备注：
 *
 */
@Service
public class Sample1Service {
	
	@Autowired
	private Sample1DAO sample1DAO;
	@Autowired
	private Sample1Mapper sample1Mapper;
	
	/**
	 * 
	 * 方法名：  getTestTableCount 
	 * 描述：  查询测试表存在的数量
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午1:24:45
	 * @return
	 *
	 */
	public Long getTestTableCount() {
		return sample1DAO.selectTestTableCount();
	}
	
	/**
	 * 
	 * 方法名：  addTestTable 
	 * 描述：  新建测试表
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午2:04:15
	 *
	 */
	public void addTestTable() {
		sample1DAO.createTestTable();
	}
	
	/**
	 * 
	 * 方法名：  dropTestTable 
	 * 描述：  删除测试表
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午2:04:52
	 *
	 */
	public void dropTestTable() {
		sample1DAO.dropTestTable();
	}
	
	/**
	 * 
	 * 方法名：  addData 
	 * 描述：  保存一条测试数据
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午3:08:53
	 *
	 */
	public void addData() {
		Sample1VO sample = new Sample1VO();
		sample.setName("新增1");
		sample1DAO.insertSample(sample);
		
//		Integer.parseInt("a");
		
		sample = new Sample1VO();
		sample.setName("新增2");
		sample1Mapper.insertSample(sample);
	}
	
	/**
	 * 
	 * 方法名：  queryData 
	 * 描述：  查询测试数据
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午3:18:31
	 * @return
	 *
	 */
	public List<Sample1VO> queryData() {
		List<Sample1VO> list = new ArrayList<Sample1VO>();
		
		List<Map<String, Object>> mapList = sample1Mapper.selectSample(new Long(0), new Long(5));
		if (CollectionUtils.isNotEmpty(mapList)) {
			Sample1VO sample;
			for (Map<String, Object> map : mapList) {
				sample = new Sample1VO();
				sample.setId(MapUtils.getLong(map, "ID"));
				sample.setName(MapUtils.getString(map, "NAME"));
				sample.setCreateTime(MapUtils.getString(map, "CREATE_TIME"));
				list.add(sample);
			}
		}
		
		return list;
	}
	
	/**
	 * 
	 * 方法名：  modifyData 
	 * 描述：  修改测试数据
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午4:24:17
	 *
	 */
	public void modifyData() {
		Sample1VO sample = new Sample1VO();
		sample.setName("已改");
		sample1Mapper.updateSample(sample);
	}
	
	/**
	 * 
	 * 方法名：  removeData 
	 * 描述：  删除测试数据
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午4:30:54
	 *
	 */
	public void removeData() {
		sample1Mapper.deleteSample(new Long(0));;
	}

}
