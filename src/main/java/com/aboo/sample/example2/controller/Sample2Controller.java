package com.aboo.sample.example2.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.aboo.framework.constant.StatusEnum;
import com.aboo.framework.vo.ReturnVO;
import com.aboo.sample.example2.pojo.Sample2VO;
import com.aboo.sample.example2.service.Sample2Service;

/**
 * 
 * 项目名称：  ttsframe-web   
 * 类名称：  SampleController   
 * 描述：  示例Controller，仅为测试服务
 * @author  娄安   
 * 创建时间：  2015年10月21日 上午8:47:41 
 * 修改人：娄安    修改日期： 2015年10月21日
 * 修改备注：
 *
 */
@Controller
@RequestMapping("/sample2")
public class Sample2Controller {
	
	private static final Logger logger= Logger.getLogger(Sample2Controller.class);
	@Autowired
	private Sample2Service sample2Service;
	
	/**
	 * 
	 * 方法名：  login 
	 * 描述：  跳转至测试首页面
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午2:05:25
	 * @param request
	 * @param response
	 * @return
	 *
	 */
	@RequestMapping("/index.do")
	public ModelAndView login(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView();
		logger.info("转入测试首页！");
		mv.setViewName("sample/index");
		return mv;
	}
	
	/**
	 * 
	 * 方法名：  checkTableExist 
	 * 描述：  检查测试表是否存在
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午2:05:40
	 * @param request
	 * @param response
	 * @return
	 *
	 */
	@RequestMapping("/checkTableExist.do")
	@ResponseBody
	public ReturnVO<Object> checkTableExist(HttpServletRequest request, HttpServletResponse response) {
		ReturnVO<Object> rv = new ReturnVO<Object>();
		Long count = sample2Service.getTestTableCount();
		if (count.longValue() > 0) {
			rv.setFlag(StatusEnum.SUCCESS);
		} else {
			rv.setFlag(StatusEnum.FAILURE);
		}
		
		return rv;
	}
	
	/**
	 * 
	 * 方法名：  createTable 
	 * 描述：  新增测试表
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午2:07:28
	 * @param request
	 * @param response
	 * @return
	 *
	 */
	@RequestMapping("/createTable.do")
	@ResponseBody
	public ReturnVO<Object> createTable(HttpServletRequest request, HttpServletResponse response) {
		ReturnVO<Object> rv = new ReturnVO<Object>();
		sample2Service.addTestTable();
		rv.setFlag(StatusEnum.SUCCESS);
		
		return rv;
	}
	
	/**
	 * 
	 * 方法名：  dropTable 
	 * 描述：  删除测试表
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午2:07:39
	 * @param request
	 * @param response
	 * @return
	 *
	 */
	@RequestMapping("/dropTable.do")
	@ResponseBody
	public ReturnVO<Object> dropTable(HttpServletRequest request, HttpServletResponse response) {
		ReturnVO<Object> rv = new ReturnVO<Object>();
		sample2Service.dropTestTable();
		rv.setFlag(StatusEnum.SUCCESS);
		
		return rv;
	}
	
	/**
	 * 
	 * 方法名：  addData 
	 * 描述：  保存一条测试数据
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午3:09:29
	 * @param request
	 * @param response
	 * @return
	 *
	 */
	@RequestMapping("/addData.do")
	@ResponseBody
	public ReturnVO<Object> addData(HttpServletRequest request, HttpServletResponse response) {
		ReturnVO<Object> rv = new ReturnVO<Object>();
		sample2Service.addData();
		rv.setFlag(StatusEnum.SUCCESS);
		
		return rv;
	}
	
	/**
	 * 
	 * 方法名：  queryData 
	 * 描述：  查询最新5条测试数据
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午3:20:01
	 * @param request
	 * @param response
	 * @return
	 *
	 */
	@RequestMapping("/queryData.do")
	@ResponseBody
	public ReturnVO<List<Sample2VO>> queryData(HttpServletRequest request, HttpServletResponse response) {
		ReturnVO<List<Sample2VO>> rv = new ReturnVO<List<Sample2VO>>();
		List<Sample2VO> datas = sample2Service.queryData();
		rv.setFlag(StatusEnum.SUCCESS);
		rv.setData(datas);
		
		return rv;
	}
	
	/**
	 * 
	 * 方法名：  updateData 
	 * 描述：  修改测试数据
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午4:25:52
	 * @param request
	 * @param response
	 * @return
	 *
	 */
	@RequestMapping("/modifyData.do")
	@ResponseBody
	public ReturnVO<Object> modifyData(HttpServletRequest request, HttpServletResponse response) {
		ReturnVO<Object> rv = new ReturnVO<Object>();
		sample2Service.modifyData();
		rv.setFlag(StatusEnum.SUCCESS);
		
		return rv;
	}
	
	/**
	 * 
	 * 方法名：  removeData 
	 * 描述：  删除测试数据
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午4:38:58
	 * @param request
	 * @param response
	 * @return
	 *
	 */
	@RequestMapping("/removeData.do")
	@ResponseBody
	public ReturnVO<Object> removeData(HttpServletRequest request, HttpServletResponse response) {
		ReturnVO<Object> rv = new ReturnVO<Object>();
		sample2Service.removeData();
		rv.setFlag(StatusEnum.SUCCESS);
		
		return rv;
	}
	
}
