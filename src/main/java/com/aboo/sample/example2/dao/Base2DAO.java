package com.aboo.sample.example2.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * 
 * 项目名称：  ttsframe-web   
 * 类名称：  BaseDAO   
 * 描述：  所有DAO的父类
 * @author  娄安   
 * 创建时间：  2015年10月21日 上午8:47:13 
 * 修改人：娄安    修改日期： 2015年10月21日
 * 修改备注：
 *
 */
@Repository
public class Base2DAO {

	@Autowired
	@Qualifier("jdbcTemplate2")
	protected JdbcTemplate jdbcTemplate;
	
}
