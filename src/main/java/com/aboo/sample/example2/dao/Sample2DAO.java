package com.aboo.sample.example2.dao;

import org.springframework.stereotype.Repository;

import com.aboo.sample.example2.pojo.Sample2VO;

/**
 * 
 * 项目名称：  ttsframe   
 * 类名称：  SampleDAO   
 * 描述：  示例DAO
 * @author  娄安   
 * 创建时间：  2015年10月13日 上午10:55:47 
 * 修改人：娄安    修改日期： 2015年10月13日
 * 修改备注：
 *
 */
@Repository
public class Sample2DAO extends Base2DAO {
	
	/**
	 * 
	 * 方法名：  selectTestTableCount 
	 * 描述：  查询测试表存在的数量
	 * @author  娄安   
	 * 创建时间：2015年10月13日 上午10:55:58
	 * @return
	 *
	 */
	public Long selectTestTableCount() {
		String sql = "SELECT COUNT(*) FROM USER_TABLES WHERE TABLE_NAME='TEST_SAMPLE'";
		return jdbcTemplate.queryForObject(sql, Long.class);
	}
	
	/**
	 * 
	 * 方法名：  createTestTable 
	 * 描述：  创建一张测试表
	 * @author  娄安   
	 * 创建时间：2015年10月13日 上午11:04:49
	 *
	 */
	public void createTestTable() {
		String sql = "CREATE TABLE TEST_SAMPLE(ID NUMBER,NAME VARCHAR2(10),CREATE_TIME DATE)";
		jdbcTemplate.execute(sql);
	}
	
	/**
	 * 
	 * 方法名：  dropTestTable 
	 * 描述：  删除测试表
	 * @author  娄安   
	 * 创建时间：2015年10月13日 上午11:06:24
	 *
	 */
	public void dropTestTable() {
		String sql = "DROP TABLE TEST_SAMPLE";
		jdbcTemplate.execute(sql);
	}
	
	/**
	 * 
	 * 方法名：  insertSample 
	 * 描述：  插入数据
	 * @author  娄安   
	 * 创建时间：2015年11月25日 下午7:25:18
	 * @param param
	 *
	 */
	public void insertSample(Sample2VO param) {
		String sql = "INSERT INTO TEST_SAMPLE(ID,NAME,CREATE_TIME) ";
		sql += "VALUES((SELECT NVL(MAX(ID),0)+1 FROM TEST_SAMPLE),?,SYSDATE)";
		jdbcTemplate.update(sql, param.getName());
	}

}
