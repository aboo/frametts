package com.aboo.sample.example2.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.aboo.sample.example2.pojo.Sample2VO;

/**
 * 
 * 项目名称：  ttsframe   
 * 类名称：  SampleMapper   
 * 描述：  示例Mapper
 * @author  娄安   
 * 创建时间：  2015年10月13日 上午11:14:14 
 * 修改人：娄安    修改日期： 2015年10月13日
 * 修改备注：
 *
 */
@Repository
public interface Sample2Mapper {
	
	/**
	 * 
	 * 方法名：  insertSample 
	 * 描述：  向示例表中插入一条记录
	 * @author  娄安   
	 * 创建时间：2015年10月13日 上午11:14:35
	 * @param param
	 *
	 */
	public void insertSample(@Param("param") Sample2VO param);
	
	/**
	 * 
	 * 方法名：  deleteSample 
	 * 描述：  从示例表中删除一条记录
	 * @author  娄安   
	 * 创建时间：2015年10月13日 上午11:15:02
	 * @param sampleId
	 *
	 */
	public void deleteSample(@Param("sampleId") Long sampleId);
	
	/**
	 * 
	 * 方法名：  updateSample 
	 * 描述：  更新示例表中一条记录
	 * @author  娄安   
	 * 创建时间：2015年10月13日 上午11:15:18
	 * @param param
	 *
	 */
	public void updateSample(@Param("param") Sample2VO param);
	
	/**
	 * 
	 * 方法名：  selectSample 
	 * 描述：  从示例表中查询数据
	 * @author  娄安   
	 * 创建时间：2015年10月13日 上午11:16:57
	 * @param minIndex
	 * @param maxIndex
	 * @return
	 *
	 */
	public List<Map<String, Object>> selectSample(@Param("minIndex") Long minIndex, 
			@Param("maxIndex") Long maxIndex);

}
