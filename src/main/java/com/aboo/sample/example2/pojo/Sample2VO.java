package com.aboo.sample.example2.pojo;

/**
 * 
 * 项目名称：  ttsframe   
 * 类名称：  SampleVO   
 * 描述：  示例VO
 * @author  娄安   
 * 创建时间：  2015年10月13日 上午11:09:04 
 * 修改人：娄安    修改日期： 2015年10月13日
 * 修改备注：
 *
 */
public class Sample2VO {
	
	private Long id;
	private String name;
	private String createTime;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

}
