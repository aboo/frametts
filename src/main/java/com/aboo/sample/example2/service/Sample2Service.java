package com.aboo.sample.example2.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aboo.sample.example2.dao.Sample2DAO;
import com.aboo.sample.example2.dao.Sample2Mapper;
import com.aboo.sample.example2.pojo.Sample2VO;

/**
 * 
 * 项目名称：  ttsframe-web   
 * 类名称：  SampleService   
 * 描述：  示例Service，仅为测试服务
 * @author  娄安   
 * 创建时间：  2015年10月21日 上午8:48:28 
 * 修改人：娄安    修改日期： 2015年10月21日
 * 修改备注：
 *
 */
@Service
public class Sample2Service {
	
	@Autowired
	private Sample2DAO sample2DAO;
	@Autowired
	private Sample2Mapper sample2Mapper;
	
	/**
	 * 
	 * 方法名：  getTestTableCount 
	 * 描述：  查询测试表存在的数量
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午1:24:45
	 * @return
	 *
	 */
	public Long getTestTableCount() {
		return sample2DAO.selectTestTableCount();
	}
	
	/**
	 * 
	 * 方法名：  addTestTable 
	 * 描述：  新建测试表
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午2:04:15
	 *
	 */
	public void addTestTable() {
		sample2DAO.createTestTable();
	}
	
	/**
	 * 
	 * 方法名：  dropTestTable 
	 * 描述：  删除测试表
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午2:04:52
	 *
	 */
	public void dropTestTable() {
		sample2DAO.dropTestTable();
	}
	
	/**
	 * 
	 * 方法名：  addData 
	 * 描述：  保存一条测试数据
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午3:08:53
	 *
	 */
	public void addData() {
		Sample2VO sample = new Sample2VO();
		sample.setName("新增1");
		sample2DAO.insertSample(sample);
		
		//Integer.parseInt("a");
		
		sample = new Sample2VO();
		sample.setName("新增2");
		sample2Mapper.insertSample(sample);
	}
	
	/**
	 * 
	 * 方法名：  queryData 
	 * 描述：  查询测试数据
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午3:18:31
	 * @return
	 *
	 */
	public List<Sample2VO> queryData() {
		List<Sample2VO> list = new ArrayList<Sample2VO>();
		
		List<Map<String, Object>> mapList = sample2Mapper.selectSample(new Long(0), new Long(5));
		if (CollectionUtils.isNotEmpty(mapList)) {
			Sample2VO sample;
			for (Map<String, Object> map : mapList) {
				sample = new Sample2VO();
				sample.setId(MapUtils.getLong(map, "ID"));
				sample.setName(MapUtils.getString(map, "NAME"));
				sample.setCreateTime(MapUtils.getString(map, "CREATE_TIME"));
				list.add(sample);
			}
		}
		
		return list;
	}
	
	/**
	 * 
	 * 方法名：  modifyData 
	 * 描述：  修改测试数据
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午4:24:17
	 *
	 */
	public void modifyData() {
		Sample2VO sample = new Sample2VO();
		sample.setName("已改");
		sample2Mapper.updateSample(sample);
	}
	
	/**
	 * 
	 * 方法名：  removeData 
	 * 描述：  删除测试数据
	 * @author  娄安   
	 * 创建时间：2015年10月13日 下午4:30:54
	 *
	 */
	public void removeData() {
		sample2Mapper.deleteSample(new Long(0));;
	}

}
