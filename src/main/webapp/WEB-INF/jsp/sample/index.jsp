<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="../js/jquery-2.1.4.min.js"></script>
<script type="text/javascript">
function checkTableExist() {
	$.ajax({
		url:"checkTableExist.do",
		data:{  },
		type:"POST",
		dataType:'json',
		success:function(data){
			if (data.flag == "Y") {
				$("#createTableDiv").css("display", "none");
				$("#dropTableDiv").css("display", "");
				$("#operationDiv").css("display", "");
			} else {
				$("#createTableDiv").css("display", "");
				$("#dropTableDiv").css("display", "none");
				$("#operationDiv").css("display", "none");
			}
		},
		error:function(data){
   			alert("出错了！！:"+data);
		}		
	});
}

function createTable() {
	$.ajax({
		url:"createTable.do",
		data:{  },
		type:"POST",
		dataType:'json',
		success:function(data){
			checkTableExist();
		},
		error:function(data){
   			alert("出错了！！:"+data);
		}		
	});
}

function dropTable() {
	$.ajax({
		url:"dropTable.do",
		data:{  },
		type:"POST",
		dataType:'json',
		success:function(data){
			checkTableExist();
		},
		error:function(data){
   			alert("出错了！！:"+data);
		}		
	});
}

function addData() {
	$.ajax({
		url:"addData.do",
		data:{  },
		type:"POST",
		dataType:'json',
		success:function(data){
			alert("新增数据成功！");
		},
		error:function(data){
   			alert("出错了！！:"+data);
		}		
	});
}

function queryData() {
	$.ajax({
		url:"queryData.do",
		data:{  },
		type:"POST",
		dataType:'json',
		success:function(data){
			var str = "";
			for (var i = 0; i < data.data.length; i++) {
				str += "ID:" + data.data[i].id + " NAME:" + data.data[i].name + " CREATE_TIME:" + data.data[i].createTime + "\n";
			}
			alert(str);
		},
		error:function(data){
   			alert("出错了！！:"+data);
		}		
	});
}

function modifyData() {
	$.ajax({
		url:"modifyData.do",
		data:{  },
		type:"POST",
		dataType:'json',
		success:function(data){
			alert("修改数据成功！");
		},
		error:function(data){
   			alert("出错了！！:"+data);
		}		
	});
}

function removeData() {
	$.ajax({
		url:"removeData.do",
		data:{  },
		type:"POST",
		dataType:'json',
		success:function(data){
			alert("删除数据成功！");
		},
		error:function(data){
   			alert("出错了！！:"+data);
		}		
	});
}

$(document).ready(function(){
	checkTableExist();
});
</script>
</head>
<body>
<div id="createTableDiv" style="display: none;">
	<input type="button" onclick="createTable()" value="建立测试表" />
</div>
<div id="dropTableDiv" style="display: none;">
	<input type="button" onclick="dropTable()" value="删除测试表" />
</div>
<div id="operationDiv" style="display: none;">
	<input type="button" onclick="addData()" value="新增数据" />
	<input type="button" onclick="queryData()" value="查询数据" />
	<input type="button" onclick="modifyData()" value="修改数据" />
	<input type="button" onclick="removeData()" value="删除数据" />
</div>
</body>
</html>